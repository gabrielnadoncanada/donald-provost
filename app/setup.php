<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use StoutLogic\AcfBuilder\FieldsBuilder;


/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_style('sage/slick-theme.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', false, null);
    wp_enqueue_style('sage/slick.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', false, null);
    wp_enqueue_script('sage/slick.js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], null, true);
    wp_enqueue_script('royalquebec/aos.js', 'https://unpkg.com/aos@2.3.1/dist/aos.js', ['jquery'], null, true);



    wp_enqueue_style('royalquebec/aos.css', 'https://unpkg.com/aos@2.3.1/dist/aos.css', false, null);
//    wp_enqueue_script('sage/parallax.js', 'https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js', ['jquery'], null, false);


//    if (is_single() && comments_open() && get_option('thread_comments')) {
//        wp_enqueue_script('comment-reply');
//    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
//    add_theme_support('soil-clean-up');
//    add_theme_support('soil-jquery-cdn');
//    add_theme_support('soil-nav-walker');
//    add_theme_support('soil-nice-search');
//    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage'),
        'footer_navigation' => __('Footer Navigation', 'sage'),
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    add_theme_support('custom-logo');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);


    add_image_size('image_featured', 450, 750, true);
    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

//    remove_theme_support('core-block-patterns');
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ];
    register_sidebar([
            'name' => __('Primary', 'sage'),
            'id' => 'sidebar-primary'
        ] + $config);
    register_sidebar([
            'name' => __('Footer', 'sage'),
            'id' => 'sidebar-footer'
        ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {

    add_image_size( 'project-size', 973, 727, true );
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });

    /**
     * Initialize ACF Builder
     */
    add_action('init', function () {
        if (class_exists('ACF')) {
            collect(glob(config('theme.dir') . '/app/fields/*.php'))->map(function ($field) {
                return require_once($field);
            })->map(function ($field) {
                if ($field instanceof FieldsBuilder) {
                    acf_add_local_field_group($field->build());
                }
            });
        }


        $post_supports = array('comments', 'editor', 'author', 'thumbnail');
        $post_types = array('page', 'post');

        foreach ($post_types as $type) {
            foreach ($post_supports as $support) {
                remove_post_type_support($type, $support);
            }
        }
    });


    init_modules();

});

function remove_wp_block_library_css()
{

}

add_action('wp_enqueue_scripts', function () {
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
    wp_dequeue_style('wc-block-style');
    wp_dequeue_style('global-styles');
});


add_action('template_redirect', function () {
    if (is_404()):
        wp_safe_redirect(home_url('/'));
        exit;
    endif;
});


//
//add_action( 'admin_init', function () {
//    remove_menu_page( 'edit-comments.php' );
//    remove_menu_page( 'edit.php?post_type=acf-field-group' );
//} );
//
//
//add_action( 'wp_before_admin_bar_render', function () {
//    global $wp_admin_bar;
//    $wp_admin_bar->remove_menu('comments');
//} );




add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/project/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => function (\WP_REST_Request $request) {

            $post_id = $request->get_param('id');
            $post_obj = get_post($post_id);

            $post_obj->chambre = get_field('Chambre', $post_id);
            $post_obj->salle_de_bain = get_field('Salle de bain', $post_id);
            $post_obj->garage = get_field('Garage', $post_id);
            $post_obj->galerie = get_field('galerie', $post_id);
            $post_obj->region = get_field('region', $post_id);
            $post_obj->architecte = get_field('Architecte', $post_id);


            wp_send_json($post_obj);
        }
    ));
});

