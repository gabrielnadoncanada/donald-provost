<?php

namespace App;

class team
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Équipes', 'team'),
            'singular_name' => __('Équipe', 'team'),
            'menu_name' => __('Équipes', 'team'),
            'all_items' => __('Tout les Équipes', 'team'),
            'view_item' => __('Voir tout les Équipes', 'team'),
            'add_new_item' => __('Ajouter un nouveau Équipe', 'team'),
            'add_new' => __('Ajouter', 'team'),
            'edit_item' => __('Éditer', 'team'),
            'update_item' => __('Modifier', 'team'),
            'search_items' => __('Rechercher', 'team'),
            'not_found' => __('Aucun résultat', 'team'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'team'),
        );

        $args = array(
            'label' => __('Équipe', 'team'),
            'description' => __('Équipes', 'team'),
            'rewrite' => ['slug' => __('equipes', 'team')],
            'labels' => $labels,
            'supports' => array('title', 'excerpt', 'thumbnail'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => false,
        );
        register_post_type('team', $args);
    }


}
new team();
