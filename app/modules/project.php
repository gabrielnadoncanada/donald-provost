<?php

namespace App;

class project
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_action("wp_footer", [$this, "project_js"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Projets', 'project'),
            'singular_name' => __('Projet', 'project'),
            'menu_name' => __('Projets', 'project'),
            'all_items' => __('Tout les Projets', 'project'),
            'view_item' => __('Voir tout les Projets', 'project'),
            'add_new_item' => __('Ajouter un nouveau Projet', 'project'),
            'add_new' => __('Ajouter', 'project'),
            'edit_item' => __('Éditer', 'project'),
            'update_item' => __('Modifier', 'project'),
            'search_items' => __('Rechercher', 'project'),
            'not_found' => __('Aucun résultat', 'project'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'project'),
        );

        $args = array(
            'label' => __('Projet', 'project'),
            'description' => __('Projets', 'project'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('projects', 'project')],
            'supports' => array('title', 'thumbnail', 'excerpt'),
            'taxonomies'  => array( 'project_category' ),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => false,
        );

        register_post_type('project', $args);

        register_taxonomy('project_category', ['book'], [
            'label' => __('Categorie(s)', 'project'),
            'hierarchical' => true,
            'rewrite' => ['slug' => 'category'],
            'show_admin_column' => true,
            'show_in_rest' => true,
        ]);

        register_taxonomy_for_object_type('project_category', 'project');
    }

    public function project_js()
    {
        ?>
        <script>
            (function ($) {
                if ($('[data-cat]').length) {

                    $('[data-cat]').click(function () {
                        $('[data-cat]').removeClass('active');
                        // location.hash = "part5";

                        if (!$(this).hasClass()) {
                            $(this).addClass('active')
                        }

                        let target = $(this).data('cat');
                        $('[data-id]').each(function () {
                            if ($(this).data('id').includes(target)) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        })
                    })
                    if(location.hash) {
                        switch(location.hash) {
                            case '#construction-residentielle':
                                $('[data-cat="10"]').click();
                                break;
                            case '#construction-commercial':
                                $('[data-cat="12"]').click();
                                break;
                            case '#renovation':
                                $('[data-cat="11"]').click();
                                break;
                        }

                    }
                }
                if ($('[data-target]').length) {
                    $('[data-target]').click(function () {
                        if ($(this).data('post')) {
                            var target = $(this).data('target');
                            var post_id = $(this).data('post');
                            var host = window.location.protocol + "//" + window.location.host;

                            var request = jQuery.ajax({
                                url: host + '/wp-json/wp/v2/project/' + post_id,
                                method: 'GET',
                                dataType: 'json',
                            });

                            request.done(function (data) {
                                console.log(data)
                                function isEmpty(str) {
                                    return (!str || str.length === 0 );
                                }

                                $(target).find('[data-content]').each(function () {
                                    let el = $(this);

                                    switch (el.data('content')) {
                                        case 'chambre':

                                            if(!isEmpty(data['chambre'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['chambre']);
                                            }

                                            break;
                                        case 'salle_de_bain':
                                            if(!isEmpty(data['salle_de_bain'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['salle_de_bain']);
                                            }

                                            break;
                                        case 'region':
                                            if(!isEmpty(data['region'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['region']);
                                            }

                                            break;
                                        case 'garage':
                                            if(!isEmpty(data['garage'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['garage']);
                                            }

                                            break;
                                        case 'title':
                                            if(!isEmpty(data['post_title'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['post_title']);
                                            }

                                            break;
                                        case 'excerpt':
                                            if(!isEmpty(data['post_excerpt'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['post_excerpt']);
                                            }

                                            break;
                                        case 'architecte':
                                            if(!isEmpty(data['architecte'])){
                                                $(this).addClass('has-content');
                                                $(this).text(data['architecte']);
                                            }

                                            break;
                                        case 'galerie':
                                            if(!isEmpty(data['galerie'])){
                                                $(this).addClass('has-content');
                                                $(el).removeClass('slick-initialized');
                                                let content = '';
                                                data['galerie'].forEach(element => {
                                                    content += '<img width="973" src="' + element['url'] + '" class="">';
                                                })
                                                $('[data-content="galerie"]').on('init', function(event, slick, direction){
                                                    $('.slick-counter').html(parseInt(slick.currentSlide + 1, 10) +'<span class="border-left"></span>'+ slick.slideCount);
                                                });
                                                $('[data-content="galerie"]').on('afterChange', function(event, slick, direction){
                                                    $('.modal-footer').find('.slick-counter').html(slick.currentSlide + 1 +' / '+slick.slideCount);
                                                });

                                                el.html(content);
                                                $('[data-content="galerie"]').slick({
                                                    dots: false,
                                                    arrows: true,
                                                    slidesToShow: 1,
                                                    prevArrow: $('.previous'),
                                                    nextArrow: $('.next'),
                                                });



                                            }


                                            break;
                                        default:
                                        // code block
                                    }
                                });

                                $(target).modal('toggle')
                            });
                        }
                    })
                }

                $('.modal .close').click(() => {
                    $('.modal').modal('toggle')
                })
            })(jQuery);

        </script>

        <?php
    }
}

new project();
