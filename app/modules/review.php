<?php

namespace App;

class testimonial
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Témoignages', 'testimonial'),
            'singular_name' => __('Témoignage', 'testimonial'),
            'menu_name' => __('Témoignages', 'testimonial'),
            'all_items' => __('Tout les Témoignages', 'testimonial'),
            'view_item' => __('Voir tout les Témoignages', 'testimonial'),
            'add_new_item' => __('Ajouter un nouveau Témoignage', 'testimonial'),
            'add_new' => __('Ajouter', 'testimonial'),
            'edit_item' => __('Éditer', 'testimonial'),
            'update_item' => __('Modifier', 'testimonial'),
            'search_items' => __('Rechercher', 'testimonial'),
            'not_found' => __('Aucun résultat', 'testimonial'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'testimonial'),
        );

        $args = array(
            'label' => __('Témoignage', 'testimonial'),
            'description' => __('Témoignages', 'testimonial'),
            'rewrite' => ['slug' => __('temoignages', 'testimonial')],
            'labels' => $labels,
            'supports' => array('title'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => false,
        );
        register_post_type('review', $args);
    }


}
new testimonial();
