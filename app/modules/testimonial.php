<?php

namespace App;

class review
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Témoignages', 'review'),
            'singular_name' => __('Témoignage', 'review'),
            'menu_name' => __('Témoignages', 'review'),
            'all_items' => __('Tout les Témoignages', 'review'),
            'view_item' => __('Voir tout les Témoignages', 'review'),
            'add_new_item' => __('Ajouter un nouveau Témoignage', 'review'),
            'add_new' => __('Ajouter', 'review'),
            'edit_item' => __('Éditer', 'review'),
            'update_item' => __('Modifier', 'review'),
            'search_items' => __('Rechercher', 'review'),
            'not_found' => __('Aucun résultat', 'review'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'review'),
        );

        $args = array(
            'label' => __('Témoignage', 'review'),
            'description' => __('Témoignages', 'review'),
            'rewrite' => ['slug' => __('temoignages', 'review')],
            'labels' => $labels,
            'supports' => array('title'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => false,
        );
        register_post_type('review', $args);
    }


}
new review();
