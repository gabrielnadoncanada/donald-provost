<?php
/**
 * Global Settings Page
 */

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];


$menus = wp_get_nav_menus();
$menus = array_combine( wp_list_pluck( $menus, 'term_id' ), wp_list_pluck( $menus, 'name' ) );

$theme = new FieldsBuilder('Theme');

$theme
    ->setLocation('options_page', '==', 'theme-settings');

$theme
    ->addTab('header', [
        'placement' => 'left'
    ])
    ->addImage('logo', [
        'label' => 'logo',
        'wrapper' => $config->wrapper
    ])
    ->addSelect('header_nav', [
        'label' => 'header_nav',
        'choices' => $menus,
        'wrapper' => $config->wrapper
    ])
    ->addTab('footer', [
        'placement' => 'left'
    ])
    ->addWysiwyg('footer_col_1', [
        'label' => 'footer_col_1',
        'wrapper' => $config->wrapper
    ])
    ->addImage('footer_col_2', [
        'label' => 'footer_col_2',
        'wrapper' => $config->wrapper
    ])
    ->addSelect('footer_col_3', [
        'label' => 'footer_col_3',
        'choices' => $menus,
        'wrapper' => $config->wrapper
    ]);


if(get_modules()){
    $modules = get_modules();
    $theme->addTab('modules', [
        'placement' => 'left'
    ]);

    foreach ($modules as $key => $module){
        $theme->addTrueFalse($module['name'], ['ui' => 1]);
    }
}

return $theme;
