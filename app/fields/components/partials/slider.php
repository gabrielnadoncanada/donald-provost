<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$slider = new FieldsBuilder('slider');

$slider
    ->addTab('Slider')
        ->addRepeater('slider_slides')
            ->addImage('slider_slides_image')
        ->endRepeater();


$slider
    ->addTab('Settings')
        ->addText('slider_class', ['wrapper' => ['width' => 25]])
        ->addText('slider_slide_class', ['wrapper' => ['width' => 25]])
        ->addText('slider_slide_image_class', ['wrapper' => ['width' => 25]])
        ->addText('slider_id', ['wrapper' => ['width' => 25]]);


return $slider;
