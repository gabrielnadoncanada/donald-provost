<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$image = new FieldsBuilder('image');

$image
    ->addTab('Image')
        ->addImage('image', ['wrapper' => ['width' => 33.33]])
        ->addText('image_width', ['wrapper' => ['width' => 33.33]])
        ->addText('image_height', ['wrapper' => ['width' => 33.33]])
        ->addLink('image_link', ['wrapper' => ['width' => 33.33]])
        ->addText('class', ['wrapper' => ['width' => 33.33]])
        ->addText('id', ['wrapper' => ['width' => 33.33]]);

return $image;
