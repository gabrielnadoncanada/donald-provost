<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$button = new FieldsBuilder('button');

$button
    ->addTab('Button')
        ->addLink('button_content', ['wrapper' => ['width' => 33.33]])
        ->addText('button_class', ['wrapper' => ['width' => 33.33]])
        ->addText('button_id', ['wrapper' => ['width' => 33.33]]);

return $button;
