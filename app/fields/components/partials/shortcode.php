<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$shortcode = new FieldsBuilder('shortcode');

$shortcode
    ->addTab('Shortcode')
    ->addText('shortcode', ['wrapper' => ['width' => 50]]);


return $shortcode;
