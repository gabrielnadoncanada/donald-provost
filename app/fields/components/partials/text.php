<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$text = new FieldsBuilder('text');

$text
    ->addTab('Text')
        ->addTextarea('text_content', [
            'new_lines' => 'br'
        ]);

$text
    ->addTab('settings')
    ->addButtonGroup('text_tag', [
        'label' => 'Text tag',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => 33.33,
            'class' => '',
            'id' => '',
        ],
        'choices' => array('P','H1','H2','H3','H4','H5','H6'),
        'allow_null' => 0,
        'default_value' => 'p',
        'layout' => 'horizontal',
        'return_format' => 'value',
    ])

    ->addText('text_id', ['wrapper' => ['width' => 33.33]])
    ->addText('text_class', ['wrapper' => ['width' => 33.33]]);

return $text;
