<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$html = new FieldsBuilder('html');

$html
    ->addTab('Html')
        ->addWysiwyg('html', ['wrapper' => ['width' => 100]]);

return $html;
