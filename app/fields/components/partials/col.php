<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$col = new FieldsBuilder('col');

$col
    ->addTab('Col')
    ->addFlexibleContent('page_builder', [
        'button_label' => 'Ajouter une composante',
        'label' => ''
    ])
    ->addLayout(get_field_partial('components.partials.text'))
    ->addLayout(get_field_partial('components.partials.button'))
    ->addLayout(get_field_partial('components.partials.image'))
    ->addLayout(get_field_partial('components.partials.html'))
    ->addLayout(get_field_partial('components.partials.card'))
    ->addLayout(get_field_partial('components.partials.slider'))
    ->addLayout(get_field_partial('components.partials.shortcode'));

$col
    ->addTab('Settings')
    ->addText('class', ['wrapper' => ['width' => 33.33]])
    ->addText('inner_class', ['wrapper' => ['width' => 33.33]])
    ->addText('id', ['wrapper' => ['width' => 33.33]]);


return $col;
