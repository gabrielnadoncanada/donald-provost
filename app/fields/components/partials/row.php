<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$row = new FieldsBuilder('row');

$row
    ->addTab('Row')
    ->addFlexibleContent('page_builder', [
        'button_label' => 'Ajouter une colonne',
        'label' => '',
    ])
    ->addLayout(get_field_partial('components.partials.col'));

$row
    ->addTab('Settings')
    ->addText('class', ['wrapper' => ['width' => 25]])
    ->addText('id', ['wrapper' => ['width' => 25]]);

return $row;
