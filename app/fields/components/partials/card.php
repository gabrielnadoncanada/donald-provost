<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 33.33],
];


$card = new FieldsBuilder('card');

$card
    ->addTab('Card')
    ->addFlexibleContent('page_builder', ['button_label' => 'Add component'])
    ->addLayout(get_field_partial('components.partials.text'))
    ->addLayout(get_field_partial('components.partials.button'))
    ->addLayout(get_field_partial('components.partials.image'))
    ->addLayout(get_field_partial('components.partials.html'))
    ->addLayout(get_field_partial('components.partials.slider'))
    ->addLayout(get_field_partial('components.partials.shortcode'));

$card
    ->addFields(get_field_partial('components.partials.image'))
    ->addTab('Settings')
    ->addText('card_class', ['wrapper' => ['width' => 33.33]])
    ->addText('card_body_class', ['wrapper' => ['width' => 33.33]])
    ->addText('card_id', ['wrapper' => ['width' => 33.33]]);



return $card;
