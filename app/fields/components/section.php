<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$section = new FieldsBuilder('section');

$section
    ->addTab('Section')
    ->addFlexibleContent('page_builder', [
        'button_label' => 'Ajouter une section',
        'label' => ''
    ])
    ->addLayout(get_field_partial('components.partials.row'));

$section
    ->addTab('settings')
    ->addText('section_class', ['wrapper' => ['width' => 25]])
    ->addText('section_id', ['wrapper' => ['width' => 25]])
    ->addSelect('section_container', ['wrapper' => ['width' => 25], 'choices' => ['Fixed', 'Fluid']])
    ->addTrueFalse('hidden', ['wrapper' => ['width' => 25]]);


return $section;
