<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$html = new FieldsBuilder('html');

$html
    ->addTab('content')
    ->addWysiwyg('content');

$html
    ->addTab('settings')
        ->addText('section_class', ['wrapper' => ['width' => 25]])
        ->addText('section_id', ['wrapper' => ['width' => 25]])
        ->addSelect('container', ['wrapper' => ['width' => 25], 'choices' => ['Fixed', 'Fluid']])
        ->addTrueFalse('hidden', ['wrapper' => ['width' => 25]]);

return $html;
