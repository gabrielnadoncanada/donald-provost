<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder('builder');


$builder
    ->addTab('builder')
        ->addFlexibleContent('page_builder', ['button_label' => 'Add Section', 'label' => ''])
            ->addLayout(get_field_partial('components.project'))
            ->addLayout(get_field_partial('components.team'))
            ->addLayout(get_field_partial('components.job'))
            ->addLayout(get_field_partial('components.section'))
            ->addLayout(get_field_partial('components.review'))
            ->addLayout(get_field_partial('components.html'));


return $builder;
