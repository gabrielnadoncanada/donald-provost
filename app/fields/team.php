<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$team = new FieldsBuilder('team');

$team
    ->setLocation('post_type', '==', 'team');

$team
    ->addTab('general')
    ->addText('titre du poste');


return $team;
