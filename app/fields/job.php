<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$job = new FieldsBuilder('job');

$job
    ->setLocation('post_type', '==', 'job');

$job
    ->addTab('general')
    ->addSelect('job_schedule', array(
        'choices' => array('Temps plein', 'Temps partiel')
    ))
    ->addSelect('job_type', array(
        'choices' => array('Contractuel', 'Poste permanent')
    ))
    ->addText('job_area')
    ->addLink('job_link');





return $job;
