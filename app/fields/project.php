<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
    'layout' => 'block'
];

$fields = new FieldsBuilder('project');

$fields
    ->setLocation('post_type', '==', 'project');

$fields
    ->addTab('Informations')
    ->addText('region', array('wrapper'=> ['width' => 33.33]))
    ->addText('Architecte', array('wrapper'=> ['width' => 33.33]))
    ->addNumber('Chambre', array('wrapper'=> ['width' => 33.33]))
    ->addNumber('Salle de bain', array('wrapper'=> ['width' => 33.33]))
    ->addNumber('Garage', array('wrapper'=> ['width' => 33.33]))
    ->addTab('galerie')
    ->addGallery('galerie', ['return_format' => 'array']);


return $fields;
