<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$review = new FieldsBuilder('review');

$review
    ->setLocation('post_type', '==', 'review');

$review
    ->addTab('general')
        ->addText('Évaluateur')
        ->addTextarea('contenu');


return $review;
