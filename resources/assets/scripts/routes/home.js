export default {
  init() {
    if ($('.slider').length) {
      $('.slider').slick({
        dots: true,
        arrows: false,
        autoplaySpeed: 3000,
      });
    }

    if ($('.testimonials').length) {
      $('.testimonials').slick({
        dots: false,
        arrows: true,
        autoplaySpeed: 3000,
        slidesToShow: 2,
        nextArrow: jQuery('.next'),
        responsive: [
          {
            breakpoint: 1920,
            settings: {
              arrows: true,
              slidesToShow: 2,
            },
          },
          {
            breakpoint: 1024,
            settings: {
              arrows: true,
              slidesToShow: 1,
            },
          },
          {
            breakpoint: 768,
            settings: {
              arrows: true,
              slidesToShow: 1,
            },
          },
        ],
      });
    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
