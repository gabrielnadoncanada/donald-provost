export default {
  init() {
    if ($('.slider').length > 0) {
      $('.slider').slick({
        dots: false,
        arrows: false,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        centerMode: true,
        responsive: [
          {
            breakpoint: 1920,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 2,
            },
          },
          {
            breakpoint: 1024,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 2,
            },
          },
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1,
            },
          },
        ],
      });
    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
