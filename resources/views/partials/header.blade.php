<header class="banner bg-secondary py-4 px-lg-5">
    <div class="container-fluid mw-1440">

        <div class="row justify-content-between">
          <div class="col-12 col-lg-auto justify-content-between d-flex align-items-center">
            <div class="">
              @if (get_field('logo', 'option'))
                <a class="brand" href="{{ home_url('/') }}">
                  <img class="img-fluid" src="{{ get_field('logo', 'option')['url'] }}" alt="logo">
                </a>
              @else
                <a class="brand" href="{{ home_url('/') }}"> {{ get_bloginfo('name', 'display') }}</a>
              @endif
            </div>
            <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="col col-lg-9 d-flex justify-content-center justify-content-lg-end align-items-center ">
            <nav class="nav-primary navbar navbar-expand-lg">

{{--              <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse"--}}
{{--                      data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"--}}
{{--                      aria-label="Toggle navigation">--}}
{{--                <span class="navbar-toggler-icon"></span>--}}
{{--              </button>--}}
              <div class="collapse navbar-collapse" id="navbarNav">
                @if (get_field('footer_logo', 'option'))

                  <div class="ml-4 d-lg-none">
                    <a class="brand" href="{{ home_url('/') }}">
                      <img class="img-fluid" src="{{ get_field('footer_logo', 'option')['url'] }}" alt="logo">
                    </a>
                  </div>


                @endif
                @if (get_field('header_nav', 'option'))
                  {{ wp_nav_menu(array(
                    'menu_id' => 'primary-navigation',
                    'menu' => get_field('header_nav', 'option'),
                    'container' => 'nav',
                    ))
                  }}
                @endif
              </div>
            </nav>
          </div>

        </div>


    </div>
</header>
