@if($page_builder)
  @foreach($page_builder as $key => $block)
    @if(!$block['hidden'])
      @php $container = 'container' @endphp
      <section id="{{ $block['section_id'] }}" class="{{ $block['section_class'] }}">
        @if(!empty($block['section_container']) && $block['section_container'] == 'Fluid')
          @php $container = 'container-fluid' @endphp
        @endif
        <div class="{{ $container }}">
          @include('partials.page-builder.' . str_replace('_', '-', $block['acf_fc_layout']))
        </div>
      </section>
    @endif
  @endforeach
@endif


