<footer class="bg-secondary pt-5 px-lg-5">
    <div class="container-fluid  mw-1440">

        <div class="row">
            <div class="col-sm-12 col-md-4 mb-4  text-center text-md-left">
                @if (get_field('footer_col_1', 'option'))
                    {!! get_field('footer_col_1', 'option') !!}
                @endif

            </div>

            <div class="col-sm-12 col-md-4 mb-4 d-flex justify-content-center">
                @if (get_field('footer_col_2', 'option'))
                    <a class="brand" href="{{ home_url('/') }}">
                        <img class="img-fluid" src="{{ get_field('footer_col_2', 'option')['url'] }}" alt="logo">
                    </a>
                @endif
            </div>
            <div
                    class="col-sm-12 col-md-4 d-flex text-center text-md-left justify-content-center justify-content-md-end mb-4 ">
                @if (get_field('footer_col_3', 'option'))
                    {!! wp_nav_menu(get_field('footer_col_3', 'option')) !!}
                @endif
            </div>
        </div>


    </div>
</footer>


<script>
    (function ($) {
        $(document).ready(function(){
            AOS.init();
        })




    })(jQuery);

</script>


