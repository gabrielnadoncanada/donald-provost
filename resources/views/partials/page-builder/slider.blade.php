{{--slider_slides--}}
{{--slider_slides--}}
{{--slider_slides_image--}}


<div class="container-fluid px-0">
  <div class="slider {{$block['slider_class']}}">
    @foreach($block['slider_slides'] as $slide)
      <div class="slider_slide {{$block['slide_class']}}">
        <img
          src="{{$slide['slider_slides_image']['url'] ?  $slide['slider_slides_image']['url'] : 'https://via.placeholder.com/1310x804'}}"
          alt="" class="img-fluid">
      </div>
    @endforeach
  </div>
</div>



