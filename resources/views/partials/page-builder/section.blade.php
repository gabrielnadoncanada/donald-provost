

@if($block['page_builder'])
  @foreach($block['page_builder'] as $row)
      <div class="row {{ $row['class'] }}" id="{{ $row['id'] }}">
        @foreach($row['page_builder'] as $col)
          <div class="col-12 {{ $col['class'] }}" id="{{ $col['id'] }}">
            <div class="col-inner {{ $col['inner_class'] }}">
              @foreach($col['page_builder'] as $component)
                @include('partials.page-builder.partials.' . str_replace('_', '-', $component['acf_fc_layout']))
              @endforeach
            </div>
          </div>
        @endforeach
      </div>
  @endforeach
@endif


