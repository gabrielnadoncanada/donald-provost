@php
  $args = array('post_type' => 'team','posts_per_page' => -1);
  $query = new WP_Query($args);
@endphp


@if($query->have_posts())
  <div class="container-fluid">
    <div class="container">
      <div class="row ">
        @while($query->have_posts())
          @php
            $query->the_post();

          @endphp
          <div class="col-sm-12 col-md-6 col-lg-4 px-4 mb-3 mb-md-5">
            <div class="card border-0">
              <img class="card-img-top" src="https://via.placeholder.com/280x340" alt="">
              <div class="card-body px-0">
                <p class="display-11 fw-700 mb-1">
                  {{ get_the_title() }}
                </p>
                <p class="display-11 fw-700 text-uppercase text-primary">
                  {{ get_field('titre du poste') }}
                </p>
                <p class="display-16">
                  {{ get_the_excerpt() }}
                </p>
              </div>
            </div>
          </div>
        @endwhile
        {{ wp_reset_postdata() }}

      </div>
      <div class="row pt-5 mt-lg-4 pb-4">
        <div class="col text-center">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="290px" height="10px" viewBox="0 0 290 10" version="1.1">
            <title>4A556235-71B0-4EE2-837A-73A566E1DE2D</title>
            <g id="Propositions" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="ÉQUIPE" transform="translate(-575.000000, -1811.000000)" fill="#BF9878">
                <path d="M865,1811 L860,1820.41051 L855,1811 L865,1811 Z M825,1811 L820,1820.41051 L815,1811 L825,1811 Z M785,1811 L780,1820.41051 L775,1811 L785,1811 Z M745,1811 L740,1820.41051 L735,1811 L745,1811 Z M705,1811 L700,1820.41051 L695,1811 L705,1811 Z M665,1811 L660,1820.41051 L655,1811 L665,1811 Z M625,1811 L620,1820.41051 L615,1811 L625,1811 Z M585,1811 L580,1820.41051 L575,1811 L585,1811 Z" id="Pattern"/>
              </g>
            </g>
          </svg>
        </div>
      </div>
    </div>
  </div>
@endif
