@php
    $args = array('post_type' => 'review','posts_per_page' => -1);
    $query = new WP_Query($args);
@endphp


@if($query->have_posts())
    <div class="container">
        <div class="row testimonials">
            @while($query->have_posts())
                @php
                    $query->the_post();

                @endphp
                <div class="col-12 col-md-6 mb-lg-4 col-lg-6 pt-3 " id="">
                    <div class="col-inner ">
                        <p class="display-9 fw-700 mb-0">{{get_the_title()}}</p>
                        @if(!empty(get_field('Évaluateur')))
                            <p class="display-13 fw-700 text-gray">

                                {{ get_field('Évaluateur') }}

                            </p>
                        @endif
                        @if(!empty(get_field('contenu')))
                            <p class="display-16 text-justify">{{get_field('contenu')}} </p>
                        @endif

                    </div>
                </div>
            @endwhile
            {{ wp_reset_postdata() }}

        </div>

    </div>
@endif
