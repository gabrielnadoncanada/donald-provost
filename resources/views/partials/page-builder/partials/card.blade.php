<div class="card {{$component['card_class']}}"  id="{{$component['card_id']}}">
  @php
    $component['class'] = 'card-img-top';
  @endphp
  @include('partials.page-builder.partials.image')
  <div class="card-body {{$component['card_body_class']}}">
      @foreach($component['page_builder'] as $component)
        @include('partials.page-builder.partials.' . str_replace('_', '-', $component['acf_fc_layout']))
      @endforeach
  </div>
</div>


