

@php
  echo '<a';
  echo $component['button_id'] ? ' id="' . $component['button_id'] . '"' : '';
  echo $component['button_class'] ? ' class="' . $component['button_class'] . '"' : '';
  echo $component['button_content']['url'] ? ' href="' . $component['button_content']['url'] . '"' : '';
  echo $component['button_content']['target'] ? ' target="' . $component['button_content']['target'] . '"' : '';
  echo '>';
  echo $component['button_content']['title'];
  echo ' </a>';
@endphp
