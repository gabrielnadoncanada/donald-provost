
@php
  echo '<'.$component['text_tag'];
  echo $component['text_id'] ? ' id="' . $component['text_id'] . '"' : '';
  echo $component['text_class'] ? ' class="' . $component['text_class'] . '"' : '';
  echo '>';
  echo $component['text_content'];
  echo ' </'.$component['text_tag'].'>';
@endphp
