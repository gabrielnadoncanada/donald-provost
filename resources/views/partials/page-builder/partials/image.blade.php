@php

$link_before = $link_after = '';

if($component['image_link']){
$link_before = '<a href="'.$component['image_link']['url'].'">';

$link_after = '</a>';
}
@endphp

{!! $link_before !!}
<img
  id="{{$component['id']}}"
  class="{{$component['class']}}"
  src="{{$component['image']['url']}}"
  alt="{{$component['image']['alt']}}"
  width="{{$component['image_width']}}"
  height="{{$component['image_height']}}"
>

{!! $link_after !!}
