
<div class="slider {{$component['slider_class']}}">
  @foreach($component['slider_slides'] as $slide)
    <div class="slider_slide {{$component['slider_slide_class']}}">
      <img
        src="{{ $slide['slider_slides_image']['url'] }}"
        alt="" class="img-fluid {{$component['slider_slide_image_class']}}">
    </div>
  @endforeach
</div>
